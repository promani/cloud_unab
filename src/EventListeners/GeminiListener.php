<?php

namespace App\EventListeners;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Events;
use GeminiAPI\Client;
use GeminiAPI\Resources\Parts\TextPart;

#[AsDoctrineListener(event: Events::postPersist, priority: 500, connection: 'default')]
class GeminiListener
{
    public function postPersist(PostPersistEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Book) {
            return;
        }

        $client = new Client($_ENV['GEMINI_APIKEY']);
        $chat = $client->geminiPro()->startChat();

        $response = $chat->sendMessage(new TextPart("Dame una sinopsis breve de {$entity->getTitle()} de {$entity->getAuthor()}"))->text();
        $entity->setDescription($response);

        $response = $chat->sendMessage(new TextPart("Dame sólo el código ISBN de ese libro que deberia ser algo corto"))->text();
        $entity->setIsbn($response);

        $entityManager = $args->getObjectManager();
        $entityManager->flush();
    }

}
