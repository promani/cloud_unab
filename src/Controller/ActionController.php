<?php

namespace App\Controller;

use App\Entity\Action;
use App\Entity\Book;
use App\Entity\Task;
use App\Form\JsonType;
use App\Service\TaskHandler;
use Doctrine\Persistence\ManagerRegistry;
use GeminiAPI\Client;
use GeminiAPI\Resources\Parts\TextPart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/actions', name: 'actions_')]
class ActionController extends AbstractController
{
    #[Route('/gemini/{id}', name: 'gemini')]
    public function gemini(Book $book, ManagerRegistry $registry): Response
    {
        $client = new Client($_ENV['GEMINI_APIKEY']);

        $chat = $client->geminiPro()->startChat();

        $response = $chat->sendMessage(new TextPart("Dame una sinopsis breve de {$book->getTitle()} de {$book->getAuthor()}"))->text();
        $book->setDescription($response);

        $repo = $registry->getManager();
        $repo->persist($book);
        $repo->flush();

        return new Response('OK');
    }


}
